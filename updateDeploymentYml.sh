#!/bin/bash

# Inhalt des Shellskripts

# Datei deployment.yml aktualisieren und in updated_deployment.yml speichern
cat ./springBootBackend-deployment-service.yml | sed 's|image:.*|image: norx26/kub-climbinghall-backend:latest|' > updated_deployment.yml

# Optional: Überprüfen Sie, ob die Datei erfolgreich aktualisiert wurde
if [ -f "updated_deployment.yml" ]; then
    echo "Die Datei wurde erfolgreich aktualisiert."
else
    echo "Fehler beim Aktualisieren der Datei."
fi
